#Chat&Pay

This is the readme document about the framework and working of CHAT&PAY chatbot application

1. The chatbot facilitates the bill payment support for user and agent through WhatsApp, Web etc

##Directory Structure
```bash
 Chatbot
    |---api.json					#json file that contains endpoints of all bbps APIs used.
    |---app.py					#controller for flask service. The program that needs to be excuted to activate the service
    |---bbps.py					#connection to bbps APIs
    |---chat apis.txt					#bbps APIs description
    |---chat_core.py					#core of the bot that sends and recieves information between user and bot
    |---database.py					#program that handles all the database operations
    |---indent.py					#program that connects the intent recognition model
    |---response_manager.py				#program where all the rules and backend of the chat is defined
    |---Rule based chatbots.docx			#design document
    |---NLU					#folder for intent recoginition model
         |---config.yml				 	#configuration file
         |---credentials.yml				#credentails file
         |---endpoints.yml				#endpoint definitions
         |---data					#Training data files
              |---chat.wsd				#flow diagram uml
              |---flow.wsd				#flow digram uml
              |---nlu.yml					#training data
         |---models				
              |---nlu-20210511-132446.tar.gz		#trained model for intent recognition
                        
```

## Getting Started

####  Anaconda installation
```bash

The installation procedure of Anaconda below:

1. Download the latest version of the Anaconda.
	https://www.anaconda.com/download
2. Go to Download directory and run the file.
	# sudo sh Anaconda3-2019.03-Linux-x86_64.sh
			OR
	# sudo ./Anaconda3-2019.03-Linux-x86_64.sh
3. Test the installation.
	# conda list
4. You can create Anaconda environments with the conda create command.
	# conda create --name chat python=3.6
5. Activate the new environment.
	# conda activate chat
6. Deactivate the new environment.
	#conda deactivate
7. Check the environment list.
	# conda info --envs
8. Remove the conda environment.
	## conda env remove -n chat
			
```

#### Installation

Run the following command through command line from the project folder:
```bash
    cd (path to project folder)
	pip install -r requirements.
```
Note: If any error in installing as pip not found, Use the command:
```bash
	conda install pip
```

##Usage

#### APIs

- Intent identification

End Point
```python
[GET] /model/parse
```

Response Format
```json
{
    "text": "string",
    "intent": {
        "id": int,
        "name": "string",
        "confidence": float
    },
    "entities": [],
    "intent_ranking": [
        {
            "id": int,
            "name": "string",
            "confidence": float
        }
    ],
    "response_selector": {
        "all_retrieval_intents": [],
        "default": {
            "response": {
                "id": null,
                "responses": null,
                "response_templates": null,
                "confidence": 0.0,
                "intent_response_key": null,
                "utter_action": "utter_None",
                "template_name": "utter_None"
            },
            "ranking": []
        }
    }
}
```

- Chat API

End Point
```python
[GET] /chatandpay
```

Request Format
```json

{
    "sender_id":"string",
    "text":"string"
}

Response Format
```json

{
    "sender_id":"string",
    "response":"string"
}

```

####To run the server

- NLU server

```bash

cd NLU
rasa run --enable-api -m models/<model_name>

```

- Chat server

```bash
cd Chatbot
python app.py

```





