import random
import indent
import requests
import bbps
import database
available_intents = [
    "pay_bill",
    "customer_service",
    "user_entry_customer",
    "user_entry_agent",
    "user_mobile_number",
    "user_otp",
    "greet",
    "goodbye",
    "affirm",
    "deny",
    "bill_selection",
    "bill_name",
    "fallback_intent"
]

available_actions = [
    "utter_greet",
    "utter_ask_user",
    "utter_ask_mobile",
    "utter_ask_otp",
    "utter_ask_category",
    "utter_ask_biller_name",
    "utter_ask_customer_params",
    "show_bill_details",
    "show_payment_link",
    "show_category",
]



#response generation
def utter_greet(sender_id):
    #updating state
    state = 'greet'
    database.update_state(sender_id, state)
    # Storing the bank of responses
    return random.choice(
        [
        "Hi! Welcome to Chat and Pay. How may I assist you today?",
        "Hello. How may I be of help?",
        "Hai, Welcome to chat and pay. How can i help you?",
        "Hello, How may i assist you?"
        ]
    )

def utter_ask_mobile(sender_id):
    state = 'ask_mobile'
    database.update_state(sender_id, state)
    return random.choice(
        [
            "Please enter your mobile number",
            "Please provide your mobile number",
            "Please enter your phone number"
        ]
    )

def utter_ask_user(sender_id):
    state = 'ask_user_type'
    database.update_state(sender_id, state)
    return random.choice(
        [
            "Please enter whether you are a customer or an agent",
            "Are you a customer or an agent"
        ]
    )

def utter_ask_otp(sender_id):
    state = 'ask_otp'
    database.update_state(sender_id, state)
    mobile_no = "8296889828"
    return random.choice(
        [
            "Please enter the otp received on "+mobile_no,
            "Enter otp recieved on " +mobile_no
        ]
    )

def show_category(sender_id):
    token = database.check_value_in_database(sender_id, "token")
    if token["status"] == "Success":
        category = bbps.category()
        reply = random.choice(
            [
                "The available categories for bill payment are: \n"+category+ "\n Please choose required bill category"
            ]
        )
    else:
        reply = utter_ask_user(sender_id)
    return reply

def otp_verifiy(sender_id):
    mobile_no = database.check_value_in_database(sender_id, "mobile_no")
    if mobile_no["status"] == "success":
        otp = "010203" #example
        login = bbps.user_login(otp, mobile_no)
        if login == "success":
            token = "eghshjs" #example
            reply = show_category(sender_id)
        else:
            reply = random.choice(
                [
                    "Incorrect OTP. Please enter correctly",
                    "OTP is incorrect, Please enter the correct OTP"
                ]
            )
    else:
        reply = utter_ask_mobile(sender_id)



def utter_ask_biller_name(sender_id):
    category = "LPG Gas" # Example value
    biller_name = bbps.biller_name_display(category)
    return random.choice(
        [
            "Please select and enter the biller name from below:\n"+biller_name
        ]
    )
    
def utter_ask_customer_params(sender_id):
    biller_name = "BPCL" # Example value
    category = "LPG Gas"
    biller_id = bbps.get_biller_id(category, biller_name)
    return biller_id


def default_fallback(sender_id):
    state = 'fallback'
    database.update_state(sender_id, state)
    return random.choice(
        [
            "Sorry i didn't understand ! Could you rephrase?",
            "Trouble in understanding ! Could you please reframe the query?",
            "Not able to process this at this moment, Please rephrase the query?"
        ]   
    )
    






# response rules
def response_selector(intent, sender_id, user_input):
    response = {}
    response['sender_id'] = sender_id
    if intent in available_intents:
        if intent == 'greet':
            response['response'] = utter_greet(sender_id)
        elif intent == 'pay_bill':
            response['response'] = show_category(sender_id) 
        elif intent == 'user_entry_customer':
            response['response'] = utter_ask_mobile(sender_id)
        elif intent == 'user_mobile_number':
            response['response'] = utter_ask_otp(sender_id)
        elif intent == 'user_otp':
            response['response'] = show_category(sender_id)
        elif intent == 'fallback_intent':
            response['response'] = default_fallback(sender_id) 
    return response


