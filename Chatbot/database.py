import pymongo
import json
from bson import json_util

# Database connection
def database_client():
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = client['chat_database']
    collection = db['chatCollection']
    return collection

collection = database_client()

# Initiate chat
def initiate_chat(sender_id):
    slots = {
        "sender_id":sender_id,
        "user":"",
        "mobile_no":"",
        "otp":"",
        "password":"",
        "token":"",
        "category":"",
        "biller_name":"",
        "biller_id":"",
        "customer_params":[],
        "bill_details":[],
        "payment":""
    }
    collection.insert_one(slots)
    documents = collection.find({ "sender_id": sender_id },{"_id":0} )
    doc = list(documents)#converting to list
    if not doc:
        msg = {
                "status":"Failed",
                "code":"404",
                "message":"Chat initiation failed "
                }
    else:
        msg = {
            "status":"Success",
            "code":"200",
            "message":"Chat iniitated"
        }
    return msg



# To check the existence of send_id
def check_chat_sender_id(sender_id):
    documents = collection.find({ "sender_id": sender_id },{"_id":0} )
    doc = list(documents)#converting to list
    if not doc:
        msg = {
                "status":"Failed",
                "code":"404",
                "message":"No sender id found "
                }
    else:
        msg = {
            "status":"Success",
            "code":"200",
            "message":"Existing sender id"
        }
    return msg



# To update the state
def update_state(sender_id, state):
    documents = collection.find({ "sender_id":sender_id},{"_id":0} )
    doc = list(documents)#converting to list
    if not doc:
        msg = {
                "status":"Failed",
                "code":"404",
                "message":"invalid sender_id, couldn't update state"}
    else:
        js = json.dumps(doc,default=json_util.default)#converted to string
        info = json.loads(js, object_hook=json_util.object_hook)[0]#to convert to dictionary from string
        collection.update({"sender_id":sender_id},{"$set":{"state":state}})
        msg = {
                "status":"Success",
                "code":"200",
                "message":"state updated successfully"}
    return msg
        


def check_value_in_database(sender_id,search_item):
    documents = collection.find({ "sender_id":sender_id},{search_item:1,"_id":0})
    doc = list(documents)#converting to list
    if not doc:
        msg = {
                "status":"Failed",
                "code":"404",
                "message":"Invalid sender_id or value not found"}
    else:
        js = json.dumps(doc,default=json_util.default)#converted to string
        info = json.loads(js, object_hook=json_util.object_hook)[0]#to convert to dictionary from string
        if info[search_item] != "":
            msg = {
                    "status":"Success",
                    "code":"200",
                    "message":"Value founded"}
        else:
            msg = {
                "status":"Failed",
                "code":"404",
                "message":"no value found"}
    return msg
