import requests
import json
api_data = open('api.json',)
api_data = json.load(api_data)

def user_login_init(mobile_no):
    data ={
         "accessCode": "",
         "appSignature": "",
         "mobileNumber": mobile_no
    }
    response = requests.post(api_data['userLoginInit'],json=data)
    response = response.json()
    status = response['generalResponse']['status']
    return status

def user_login(otp,mobile_no):
    data ={
         "accessCode": otp,
         "appSignature": "",
         "mobileNumber": mobile_no
    }
    response = requests.post(api_data["userLogin"],json=data)
    response = response.json()
    status = response['generalResponse']['status']
    return status


def category():
     category = []
     response = requests.get(api_data["categories-master"])
     response = response.json()
     for categoryName in response['categoryRespList']:
          category.append(categoryName['categoryName'])
     category = "\n".join(category)
     return category

def biller_name_display(category):
     biller_name =[]
     params = {'category': category}
     response = requests.get(api_data["billers"], params = params)
     response = response.json()
     for billerName in response['billerResp']:
          biller_name.append(billerName['billerName'])
     biller_name = "\n".join(biller_name)
     return biller_name

def get_biller_id(category,biller_name):
     params = {'category': category, 'billerName':biller_name}
     response = requests.get(api_data["billers"], params = params)
     response = response.json()
     if response['billerResp']!= []:
          biller_id = response['billerResp'][0]["billerId"]
     else:
          biller_id = ""
     return biller_id

def customer_params(biller_id):
     cust_params = []
     params = {'billerId': biller_id}
     response = requests.get(api_data["customerParam"], params = params)
     response = response.json()
     if response['generalResp']['status'] == "success":
          for customParamName in response['customParamResp']:
               cust_params.append(customParamName['customParamName'])
     return cust_params

