import requests
import json

def intent_identifiy(user_input):
    payload = {'text':user_input}
    headers = {'content-type': 'application/json'}
    response = requests.post('http://localhost:5005/model/parse', json=payload, headers=headers)
    response = response.json()
    if float(response['intent']['confidence']) < 0.8:
        if response['entities'] != []:
            if response['entities'][0]['entity'] == "otp":
                intent = response['intent']['name'] = "user_otp"
            else:
                intent = response['intent']['name'] = "fallback_intent"
        else:
            intent = response['intent']['name'] = "fallback_intent"
    else:
        intent = response['intent']['name']
    return intent
