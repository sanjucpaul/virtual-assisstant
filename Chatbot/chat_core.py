import indent
import response_manager
import database
import pymongo
import json
from bson import json_util
  
def chat(user_input, sender_id):
    sender_id_status = database.check_chat_sender_id(sender_id)
    if sender_id_status['status'] == "Success":
        intent = indent.intent_identifiy(user_input)
        bot_response = response_manager.response_selector(intent, sender_id, user_input)
    else:
        database.initiate_chat(sender_id)
        intent = indent.intent_identifiy(user_input)
        bot_response = response_manager.response_selector(intent, sender_id,user_input)
    return bot_response['response']

