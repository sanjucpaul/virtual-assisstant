import flask
from flask import Flask, render_template, request, redirect, url_for, session 
from flask import Flask
import requests
import json
import uuid
import chat_core
app = Flask(__name__)


#RECTIFYING CORS PROBLEM
@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  return response


@app.route('/chatandpay', methods=['GET', 'POST'])
def chatandpay():
    msg = {}
    if request.method == 'POST' and 'sender_id' in request.json and 'text' in request.json:
        sender_id = request.json['sender_id']
        text = request.json['text']
        msg = chat_core.chat(text, sender_id)
    return msg













if __name__ == '__main__':
    app.run()